/* eslint-disable */
/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */

import { precacheAndRoute } from 'workbox-precaching'

precacheAndRoute(self.__WB_MANIFEST)

self.addEventListener('push', (event) => {
  const title = 'GetCovaxIN'
  const pushBody = event.data.text()
  const pushMap = JSON.parse(pushBody)
  const shouldPut = pushMap.should_put === undefined ? true : pushMap.shouldPut
  // console.log(shouldPut)
  // console.log('push event received')
  // pincode, request_id, date, age

  event.waitUntil(
    putNotificationReceived(pushMap.request_id)
      .then(() => {
        // console.log('put notification received')
        return getData(pushMap.pincode, pushMap.date)
      })
      .then((data) => {
        return activeCenters(data)
      })
      .then((pincodeVaccinationDetails) => {
        if (!shouldPut) {
          // console.log('skipping PUT')
          return pincodeVaccinationDetails
        }
        // console.log(shouldPut)
        // console.log('not skipping PUT')

        return fetch(`/api/pincodes/${pushMap.pincode}/${JSON.stringify(pincodeVaccinationDetails)}`, {
          method: 'PUT',
        }).then((response) => {
          return response.json()
        })
        // .catch((error) => {
        //   return {
        //     slots: {
        //       slot18: {
        //         "dose1": 1,
        //         "dose2": 0
        //       },
        //       slot45: {
        //         "dose1": 1,
        //         "dose2": 0
        //       }
        //     },
        //     center: {
        //       "c18": 42,
        //       "c45": 0
        //     },
        //     days: {
        //       "days18": 5,
        //       "days45": 0
        //     },
        //     pincode: "110011"
        //   }
        // })
      })
      .then((pincodeVaccinationDetails) => {
        // const stringPayload = JSON.stringify(pincodeVaccinationDetails)
        const slotsCenterDays = getSlotsCenterDays(pincodeVaccinationDetails, pushMap.age_group)
        let bodyString = null

        if (!!pushMap.text && slotsCenterDays.slots === 0) {
          bodyString = pushMap.text
        } else if (slotsCenterDays.slots !== 0) {
          bodyString = `${slotsCenterDays.slots} vaccination slots available for above ${pushMap.age_group} over next ${slotsCenterDays.days} days in ${slotsCenterDays.center} centers near ${pushMap.pincode}`
        } else {
          bodyString = `No slots for above ${pushMap.age_group} are available for ${pushMap.pincode} on ${pushMap.date}. Please click here to keep receiving notifications.`
        }

        const opts = {
          body: bodyString,
          icon: 'icon-128x128.png',
          badge: 'icon-64x64.png'
        }

        return self.registration.showNotification(title, opts)
      })
      .catch((error) => {
        console.log(error)
      })
  );
})

self.addEventListener('notificationClick', (event) => {
  event.notification.close()

  event.waitUntil(clients.openWindow('https://getcovax.in/'))
})

function putNotificationReceived(requestID) {
  return fetch(`/api/requests/${requestID}`, {
    method: 'PUT',
  })
}

function getSlotsCenterDays(pincodeVaccinationDetails, age) {
  return {
    slots: getSlots(pincodeVaccinationDetails, age),
    center: getCenter(pincodeVaccinationDetails, age),
    days: getDays(pincodeVaccinationDetails, age)
  }
}

function getSlots(pincodeVaccinationDetails, age) {
  const slots =
    pincodeVaccinationDetails.slots[`slot${age}`]['dose1'] +
    pincodeVaccinationDetails.slots[`slot${age}`]['dose2']
  return !!slots ? slots : 0
}

function getCenter(pincodeVaccinationDetails, age) {
  const center = pincodeVaccinationDetails.center[`c${age}`]
  return !!center ? center : 0
}

function getDays(pincodeVaccinationDetails, age) {
  const days = pincodeVaccinationDetails.days[`days${age}`]
  return !!days ? days : 0
}

function activeCenters(data) {
  let cIDMin18 = [];
  let slot18Dose1 = [];
  let slot18Dose2 = [];
  let date18 = [];

  let cIDMin45 = [];
  let slot45Dose1 = [];
  let slot45Dose2 = [];
  let date45 = [];

  // console.log(data)

  data.centers.forEach(element => {
    element.sessions.forEach(element => {
      const date = formattedDate(element.date);
      if (element.min_age_limit === 18) {
        cIDMin18.push(element.center_id);
        // slot18Dose1.push(element.available_capacity_dose1);
        // slot18Dose2.push(element.available_capacity_dose2);
        slot18Dose1.push(element.available_capacity);
        slot18Dose2.push(0);
        date18.push(date);
      } else {
        cIDMin45.push(element.center_id);
        // slot45Dose1.push(element.available_capacity_dose1);
        // slot45Dose2.push(element.available_capacity_dose2);
        slot45Dose1.push(element.available_capacity)
        slot45Dose2.push(0)
        date45.push(date);
      }
    });
  });

  const slots = {
    slot18: {
      dose1: addArrayElements(slot18Dose1),
      dose2: addArrayElements(slot18Dose2)
    },
    slot45: {
      dose1: addArrayElements(slot45Dose1),
      dose2: addArrayElements(slot45Dose2)
    }
  };
  const center = {
    c18: [... new Set(cIDMin18)].length,
    c45: [... new Set(cIDMin45)].length
  };
  const days = {
    days18: dateGap(Math.min(...date18), Math.max(...date18)),
    days45: dateGap(Math.min(...date45), Math.max(...date45))
  };

  return {
    slots: slots,
    center: center,
    days: days
  }

  // return {
  //   cIDMin18: cIDMin18,
  //   slot18Dose1: slot18Dose1,
  //   slot18Dose2: slot18Dose2,
  //   date18: date18,
  //   cIDMin45: cIDMin45,
  //   slot45Dose1: slot45Dose1,
  //   slot45Dose2: slot45Dose2,
  //   date45: date45
  // }
}

function addArrayElements(numArray) {
  let sum = 0
  numArray.forEach(element => {
    sum += element
  });

  return sum
}

function getData(pincode, date) {
  return fetch(
    `https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode=${pincode}&date=${date}`,
    {
      headers: {
        Host: 'cdn-api.co-vin.in',
        Accept: 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        Origin: 'https://www.cowin.gov.in',
        DNT: '1',
        Connection: 'keep-alive',
        Referer: 'https://www.cowin.gov.in/',
        TE: 'Trailers'
      }
    }
  ).then((response) => {
    return response.json()
  })
}

function formattedDate(date) {
  const dateArray = date.split('-');
  return Date.parse(dateArray[1] + '/' + dateArray[0] + '/' + dateArray[2]);
}

function dateGap(start, end) {
  // console.log('date gap', start, end)
  const diffTime = Math.abs(end - start);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  // console.log(diffTime.toString() + ' milliseconds', diffDays == 'Infinity');
  return diffDays == 'Infinity' ? 0 : diffDays;
}